interface Rectangle {
    width: number;
    height: number;
}

interface ColorRec extends Rectangle{
    color: string
}

const rectangle: Rectangle = {
    width: 25,
    height: 10
}
console.log(rectangle)

const colorRec: ColorRec = {
    width: 20,
    height: 10,
    color: "red"
}
console.log(colorRec)