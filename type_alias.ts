type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = {
    year: CarYear,
    type: CarType,
    model: CarModel,
}

const carYear: CarYear = 2000;
const carType: CarType = "Toyota";
const carModel: CarModel = "Corolla";

const car1: Car = {
    year: 2013,
    type: "Masda",
    model: "mini",
}
console.log(car1);