function getTime(): number {
    return new Date().getTime();
}

console.log(getTime());

function printHello(): void{
    console.log("Hello");
}
printHello();

function multimly(a: number, b: number){
    return a * b;
}

console.log(multimly(1,2));

function add(a: number, b:number, c?: number){
    return a+b+(c || 0);
}
console.log(add(1,2,3));
console.log(add(1,2));

function pow(value: number, exponent: number = 10):number {
    return value ** exponent;
}
console.log(pow(10));
console.log(pow(10,2));

function divide({dividend, divisor}: {dividend: number, divisor: number}){
    return dividend / divisor;
}
console.log(divide({dividend: 100, divisor: 10}));

function add2(a: number, b: number, ...rest: number[]){
    return a+b+rest.reduce((p,c) => p+c, 0);
}
console.log(add2(1,2,3,4,5));

type Negate = (Value: number) => number;
const negateFunction: Negate = (Value: number) => Value*-1;
const negateFunction2: Negate = function(Value: number){
    return Value*-1;
}
console.log(negateFunction(1));
console.log(negateFunction2(1));