function printStatCode(code: string | number) {
    if(typeof code == 'string'){
        console.log(`My status is ${code.toUpperCase()} ${typeof code}`);
    }else{
        console.log(`My status is ${code} ${typeof code}`);
    }
}

printStatCode(404);
printStatCode("abc");